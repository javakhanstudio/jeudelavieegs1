package jdlv;

public class Game 
{

	boolean[][] valeurs ;
	
	
	Game(int x, int y) {
		valeurs = new boolean[x][y] ;
	}
	
	
	public void calcule() {
		
		boolean[][] newValeurs = new boolean[valeurs.length][valeurs.length] ;
		
		for(int x = 0 ; x < valeurs.length ; x++) {
			for(int y = 0 ; y < valeurs[x].length ; y++) {
				newValeurs[x][y] = checkCase(x,y) ;
			}
		}
		
		valeurs = newValeurs ; 
	}


	private boolean checkCase(int x, int y) {
		int voisin = 0 ;
		
		// → 
		if(x == valeurs.length - 1) {
			
		} else {
			voisin += valeurs[x+1][y] ? 1 : 0 ; 
		}
		
		// ←
		if(x == 0) {
			
		} else {
			voisin += valeurs[x-1][y] ? 1 : 0 ; 
		}
		
		//  ↓
		if(y == valeurs[x].length - 1) {
			// todo ajouter
		} else {
			voisin += valeurs[x][y + 1] ? 1 : 0 ; 
		}
		
		// ↑
		if(y == 0) {
			// todo ajouter
		} else {
			voisin += valeurs[x][y - 1] ? 1 : 0 ; 
		}
		
	
		
//		valeurs[x+1][y+1] ;
		// ↘
		if(x == valeurs.length - 1 || y == valeurs[x].length - 1) {
			
		} else {
			voisin += valeurs[x + 1][y + 1] ? 1 : 0 ; 
		}
		
//		valeurs[x+1][y-1] ; 
		// ↗
		if(x == valeurs.length - 1 || y == 0) {
			
		} else {
			voisin += valeurs[x + 1][y - 1] ? 1 : 0 ; 
		}
		
//		valeurs[x-1][y + 1] ; 
		// ↙
		if(x == 0 || y == valeurs[x].length - 1) {
			
		} else {
			voisin += valeurs[x-1][y + 1] ? 1 : 0 ; 
		}
		
//		valeurs[x-1][y - 1] ; 
		// ↖
		if(x == 0 || y == 0) {
			
		} else {
			voisin += valeurs[x-1][y - 1] ? 1 : 0 ; 
		}
		
		if(voisin == 3)
			return true ;
		else if(voisin > 3)
			return false ;
		
		return valeurs[x][y] ; 
	}
	
}
