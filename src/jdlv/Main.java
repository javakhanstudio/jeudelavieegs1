package jdlv;

import java.util.Random;

public class Main 
{

	
	
	public static void main(String[] args)
	{
		
		Game game = new Game(30,30) ; 
		int chancePercent = 10 ; 
		//setValueHard(game) ;
		setValueRandom(game,chancePercent) ;
		
		
		while(true) {
			
			try {
				Thread.sleep(1);
				for(int x = 0 ; x < game.valeurs.length ; x++) {
					for(int y = 0 ; y < game.valeurs[x].length ; y++) {
						System.out.print(game.valeurs[x][y] ? "■" : "□");
						
						
						if(y != game.valeurs[x].length - 1)
							System.out.print("-");
					}
					System.out.println();
				}
				System.out.println("\n\n");
				game.calcule() ;
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			
			
		}
		
	}

	private static void setValueRandom(Game game, int chancePercent) 
	{
		Random random = new Random() ;
		
		for(int x = 0 ; x < game.valeurs.length ; x++) {
			for(int y = 0 ; y < game.valeurs[x].length ; y++) {
				game.valeurs[x][y] = random.nextInt(100) <= chancePercent ;
			}
		}
		
	}

	private static void setValueHard(Game game) 
	{
		game.valeurs[0][0] = true ;
		game.valeurs[0][1] = true ;
		game.valeurs[0][2] = true ;
		game.valeurs[0][3] = true ;
		game.valeurs[0][4] = true ;
		game.valeurs[0][5] = true ;
		
	}

}
